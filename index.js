/** Bài 1
 * - Đầu vào:
 * Gán giá trị điểm 3 môn
 * Gán điểm chuẩn
 * Gán điểm khu vực
 * - Xử lí:
 * Đầu tiên: nếu 3 môn = 0 thì thi trượt
 * Gán điểm cho từng Khu Vực từng Đối Tượng
 * Điểm tổng = điểm 3 môn + điểm khu vực + điểm đối tượng
 * - Đầu ra:
 * Điểm tổng >= điểm chuẩn -> thi đậu
 * Ngược lại -> rớt
 */
function ketQua1(){
    var diemChuan = document.getElementById("diemChuan").value*1
    var khuVuc = document.getElementById("khuVuc").value*1
    var doiTuong = document.getElementById("doiTuong").value*1
    var diem1 = document.getElementById("diem1").value*1
    var diem2 = document.getElementById("diem2").value*1
    var diem3 = document.getElementById("diem3").value*1
    if (diem1<=0 || diem2<=0 || diem3<=0){
       alert('Bạn đã rớt tốt nghiệp vì 1 trong 3 môn của bạn = 0')
    }
    var diemKhuVuc= khuVuc == 'A' ? 2 : khuVuc == 'B' ? 1 : khuVuc == 'C' ? 0.5 : 0 
    var diemDoiTuong = doiTuong == '1' ? 2.5 :doiTuong == '2' ? 2 : 1
    var diemTongKet = diem1 +diem2 +diem3 + diemKhuVuc + diemDoiTuong
    if (diemTongKet>=diemChuan){
        alert('Bạn đã trúng tuyển')
    } else {
        alert('Rất tiếc! Bạn đã thi trượt')
    }
}


/**Bài 2
 * - Đầu vào
 * Gán giá trị số kW nhập vào
 * Gán giá trị Tính tiền điện
 * - Xử lí
 * Tính Tiền Điện 
 * (50kW đầu =500d/kW
 * 50kW kế =650d/kW
 * 100kW đầu =850d/kW
 * 150kW đầu =110d/kW
 * còn lại = 1300d/kW)
 * - Đầu ra
 * In ra số tiền điện
 */
function soTienDien(){
    var soKw = document.getElementById("soKw").value*1
    var tienDien=document.getElementById("result2")
    var hoTen=document.getElementById("hoTen").value
    if (hoTen==='') {
        alert ('Vui Lòng Nhập Họ Và Tên')
    }
    if (soKw<=0){
        alert ('Vui lòng nhập đầy đủ thông tin!')
    } else if (soKw<=50) {
        tienDien= soKw*500
    } else if (soKw<=100){
        tienDien= (50*500) + (soKw-50)*650
    } else if (soKw<=200){
        tienDien= (50*500)+(50*650)+(soKw-100)*850
    }
    else if (soKw<=350){
        tienDien= (50*500)+(50*650)+(100*850)+(soKw-200)*1100
    } else {
        tienDien= (50*500)+(50*650)+(100*850)+(350*1100)+(soKw-350)*1300
    }
    document.getElementById("result2").innerHTML=`<p>Số tiền điện:</p>${tienDien} VND`
}

/**Bài 3
 * - Đầu vào
 * Gán họ và tên 
 * Gán số người phụ thuộc
 * Gán thu nhập hằng tháng
 * Gán tiền thuế
 * - Xử lý 
 * Thu nhập năm = thu nhập tháng *12
 * Tiền chịu thuế = thu nhập năm -4 - số người phụ thuộc *1.6;
 * Tiền thuế = tiền chịu thuế * (phần trăm để bài yêu cầu)
 * - Đầu ra
 * In số tiền thuế
 */

function soThue() {
    var hoTen =document.getElementById("hoVaTen").value
    var soNguoi= document.getElementById("soNguoiPhuThuoc").value*1
    var thuNhapThang=document.getElementById("tongThuNhap1").value*1;
    var tienThue;
    var tongThuNhapNam=thuNhapThang*12;
    var chiuThue=tongThuNhapNam - 4 - (soNguoi*1,6);
    if(hoTen ==='' || thuNhapThang === ''){
        alert ('Vui Lòng Nhập Đầy Đủ Thông Tin')
    } 
    if (chiuThue<=60)
    {
        tienThue=chiuThue*0.05
    } else if (chiuThue<=120){
        tienThue= 60*0.5 + (chiuThue-60)*0.1
    }else if (chiuThue<=210){
        tienThue= 60*0.5 + (120*0.1) + (chiuThue-120)*0.15
    }else if (chiuThue<=384){
        tienThue= 60*0.5 + (120*0.1) + (210*0.15) + (chiuThue-210)*0.2
    }else if (chiuThue<=624){
        tienThue=60*0.5 + (120*0.1) + (210*0.15) + (384*0.2) + (chiuThue-384)*0.25
    }else if (chiuThue<=960){
        tienThue= 60*0.5 + (120*0.1) + (210*0.15) + (384*0.2) + (624*0.25) + (chiuThue-624)*0.3
    }else {tienThue= 60*0.5 + (120*0.1) + (210*0.15) + (384*0.2) + (624*0.25) + (960*0.3) + (chiuThue-960)*0.35}
    document.getElementById("result3").innerHTML=`<p>Thuế thu nhập cá nhân:</p>${tienThue} triệu VND`
}
/** Bài 4:
 * Đầu vào:  
 *  - Mã khách hàng
 *  - Loại khách hàng
 *  - so ket noi
 *  - so kenh cao cap
 * Xử lý:
 * - lấy lựa chọn của user (if== nhà dân ==> ẩn số kn) else ==> hiện số kết nối
 *  if user == nhaDan => soTien = phí sử lý hóa đơn + Phí dịch vụ cơ bản + thuê kênh cao cấp * 7.5
 * else soTien == phi sử lí hóa đơn + phi dic vu co ban (if <= 10 ==> phi dich vu co ban == soketnoi *75) (else phi dich vu co ban === 10*75 + soketnoi-10)
 * Đầu ra: In ra số tiền Cáp
 */
const ketNoi = document.querySelector('.so-ket-noi')
ketNoi.style.display = 'none';
var khachHang = document.getElementById('khachHang')
khachHang.onchange = function() {
    khachHang.value;
    if (khachHang.value == 'doanhNghiep') {
        ketNoi.style.display = 'block';
    } else {
        ketNoi.style.display = 'none';

    }
}
document.getElementById('btn-4').onclick = function() {
    var soKetNoi = document.getElementById('soKetNoi').value*1
    var ketNoiDau = 7.5;
    var kenhCaoCap = document.getElementById('kenhCaoCap').value*1;
    var giaTienTrenKenh;
    var Tong;
    var tong_10ketNoiDau = 75;
    var tienKetNoi;
    if (khachHang.value == 'doanhNghiep') {
        if (soKetNoi<10) {
            tienKetNoi = soKetNoi * ketNoiDau
            giaTienTrenKenh = kenhCaoCap * 50 
            Tong = 15 + tienKetNoi + giaTienTrenKenh
        } else {
            for (var i = 1; i <= (soKetNoi-10); i++) {
            tong_10ketNoiDau +=5;
            tienKetNoi = tong_10ketNoiDau; 
            } 
        giaTienTrenKenh = kenhCaoCap * 50 
        Tong = 15 + tienKetNoi + giaTienTrenKenh
        } 
        document.getElementById('ketQua4').innerHTML = `<p>Phí Xử Lý Hóa Đơn: 15$</p>  <p>Phí Dịch Vụ Cơ Bản: ${tienKetNoi}$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${Tong}$</p>` 
    }  
    if(khachHang.value == 'nhaDan') {
            giaTienTrenKenh = kenhCaoCap * 7.5;
            Tong = giaTienTrenKenh + 4.5 + 20.5;
            document.getElementById('ketQua4').innerHTML = `<p>Phí Xử Lý Hóa Đơn: 4.5$</p>  <p>Phí Dịch Vụ Cơ Bản: 20.5$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${Tong}$</p>` 
    } 
}